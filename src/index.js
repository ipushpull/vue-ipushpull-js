import ipushpull from 'ipushpull-js';

export default {
  install(Vue, options) {
    if (options) {
      ipushpull.config.set(options);
    }
    Vue.prototype.$ipushpull = ipushpull;
  }
};
