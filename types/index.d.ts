import Vue, { PluginFunction } from 'vue';
import { IPushPull } from 'ipushpull-js';
import { IIPPConfig } from 'ipushpull-js/dist/Config';

declare module 'vue/types/vue' {
  interface Vue {
    $ipushpull: IPushPull;
  }
}

declare class ipushpull {
  constructor(options?: IIPPConfig);
  static install: PluginFunction<never>;
}

export default ipushpull;
